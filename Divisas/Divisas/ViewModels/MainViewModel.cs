﻿using Divisas.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Divisas.ViewModels
{
    public class MainViewModel: BindableObject 
    {
        #region Atributes

        private decimal dollars;

        private decimal pesos;

        private decimal euros;

        private decimal pounds;

        private DialogService dialogService;

        #endregion

        #region Constructor
        public MainViewModel()
        {
            dialogService = new DialogService();
        } 
        #endregion


        #region Properties
        public decimal Dollars
        {
            get
            {
                return dollars;
            }
            set
            {
                if (dollars != value)
                {
                    dollars = value;
                    OnPropertyChanged("Dollars");
                }
            }
        }

        public decimal Pesos
        {
            get
            {
                return pesos;
            }
            set
            {
                if (pesos!=value)
                {
                    pesos = value;
                    OnPropertyChanged("Pesos");
                }
            }
        }

        public decimal Pounds
        {
            get
            {
                return pounds;
            }
            set
            {
                if (pounds!= value)
                {
                    pounds = value;
                    OnPropertyChanged("Pounds");
                }
            }
        }

        public decimal Euros
        {
            get
            {
                return euros;
            }
            set
            {
                if (euros != value)
                {
                    euros = value;
                    OnPropertyChanged("Euros");
                }
            }
        }
        #endregion

        #region Commands
        public ICommand ConvertCommand { get { return new Command(ConvertMoney); } }

        private async void ConvertMoney()
        {
            if (Dollars <= 0)
            {
                await dialogService.ShowMessage("Error", "Se debe ingresar un valor en dolares mayor que 0");
                return;
            }
            Pesos = Dollars * (decimal)3039.51368;
            Pounds = Dollars * (decimal)0.789973654;
            Euros = Dollars * (decimal)0.897070169;
        } 
        #endregion
    }
}
